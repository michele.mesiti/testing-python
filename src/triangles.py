import math


def heron(a,b,c):
    s = 0.5*(a+b+c)
    if a > s:
        raise ValueError(f"a={a} is too large!")
    if b > s:
        raise ValueError(f"a={a} is too large!")
    return math.sqrt(s *
                     (s-a) *
                     (s-b) *
                     (s-c))
