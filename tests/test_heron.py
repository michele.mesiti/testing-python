from triangles import heron
import pytest

def test_smallest_pythagorean_triple():
    a = 3
    b = 4
    c = 5
    assert heron(a,b,c) == a*b/2


def test_heron_raises_with_c_too_large():
    a = 3
    b = 4
    c = a+b+1

    with pytest.raises(ValueError,
                       match = r"c=8 is too large!"):
        heron(a,b,c)

def test_heron_raises_with_b_too_large():
    a = 3
    c = 4
    b = a+c+1

    with pytest.raises(ValueError,
                       match = r"b=8 is too large!"):
        heron(a,b,c)
